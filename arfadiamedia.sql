/*
SQLyog Community v11.24 (32 bit)
MySQL - 5.6.16 : Database - arfadiamedia
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`arfadiamedia` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `arfadiamedia`;

/*Table structure for table `lokasi` */

DROP TABLE IF EXISTS `lokasi`;

CREATE TABLE `lokasi` (
  `id_lokasi` int(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `statename` varchar(100) DEFAULT NULL,
  `countryname` varchar(100) DEFAULT NULL,
  `images` varchar(200) DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_lokasi`),
  KEY `FK_id_properti` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `lokasi` */

insert  into `lokasi`(`id_lokasi`,`nama`,`telp`,`lat`,`lang`,`address`,`statename`,`countryname`,`images`,`video`) values (13,'ana','0823615648937','-6.238320074044168','106.82540142908691','Jalan Kuningan Barat II','Kuningan Barat','Mampang Prapatan','images/IMG_20160725_120839.jpg','images/VID_20160710_093119.mp4'),(14,'materna','082361564523','-6.239390305346969','106.82618653417926','Jalan Haji R. Rasuna Said No.3','Kuningan Barat','Mampang Prapatan','images/IMG_20160725_150736.jpg','images/VID_20160725_150741.mp4'),(15,'hujan','08236126895623','-6.238583852536977','106.82542900554837','Jalan Kuningan Barat 2 No.11','Kuningan Barat','Mampang Prapatan','images/IMG_20160726_091602.jpg','images/VID_20160726_091609.mp4'),(16,'halo','952685566285','-6.238364330492913','106.82567870244384','Jalan Kuningan Barat II','Kuningan Barat','Mampang Prapatan','images/IMG_20160726_104813.jpg','images/VID_20160726_104822.mp4');

/*Table structure for table `properti` */

DROP TABLE IF EXISTS `properti`;

CREATE TABLE `properti` (
  `id_properti` int(20) NOT NULL AUTO_INCREMENT,
  `nama_properti` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_properti`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `properti` */

insert  into `properti`(`id_properti`,`nama_properti`) values (1,'Android'),(2,'java'),(3,'IOS'),(4,'SQL'),(5,'JDBC'),(6,'Web services'),(7,'Analysis'),(8,'Wordpress'),(9,'sqlyog'),(10,'jdk');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`username`,`password`) values (1,'ana','24d4b96f58da6d4a8512313bbd02a28ebf0ca95dec6e4c86ef78ce7f01e788ac');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
