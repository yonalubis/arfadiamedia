<?php

class DbHandler {
 
    private $conn;
 
    function __construct() {
        require_once dirname(__FILE__) . './DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function getUserByUsernameAndPassword($username, $password) {
        $stmt = $this->conn->prepare("SELECT user.id_user, user.username, user.password FROM user WHERE user.username = ?");
        $stmt->bind_param("s", $username);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $encrypted_password = $user['password'];
            //$hash = $this->checkhashSSHA($password);
            $hash = hash("sha256", $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                
                // user authentication details are correct
                return "success";
            }
            else{
                return "FALSE";
                
            }
        } else {
            return "something wrong";
        }
    }
 
 	public function Login($username, $password) {
        $query = "SELECT user.id_user, user.username, user.password FROM user WHERE user.username = '$username'";
        $result = $this->conn->query($query);
        $data = $result->fetch_assoc();
        $response = $data['id_user'] . "#" . $data['username'];
        // verifying user password
            $encrypted_password = $data['password'];
            //$hash = $this->checkhashSSHA($password);
            $hash = hash("sha256", $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $response;
            }
            else{
                return "FALSE";   
            }        
    }

    public function addUser($username, $password) {
        //$hash = $this->hashSSHA($password);
        $hash= hash("sha256", $password);
        if(!$this->isUserExisted($username)){
            $stmt = $this->conn->prepare("INSERT INTO user (username, password) VALUES (?, ?)");
            $stmt->bind_param("ss",$username, $hash);
            $tasks = $stmt->execute();
            $stmt->close();
            if ($tasks) {
            return "success";
            } else {
                return "not success";
            }
        }
        else {
                return "username is already exists";
            }
    }

    public function isUserExisted($username) {
        $stmt = $this->conn->prepare("SELECT username from user WHERE username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->store_result();
        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function getProperti() {
        $ret = array();
        $query = "SELECT properti.id_properti, properti.nama_properti FROM properti";
        $result = $this->conn->query($query);
        if ($result != null) {
            $index = 0;
            while ($task = $result->fetch_assoc()) {
                $ret[$index] = $task["id_properti"] . "#" . $task["nama_properti"];
                $index++;
            }
        } else {
            $ret = "";
        }
        return $ret;
    }

    public function SaveImage($image, $name) {
        $path = "images/" . $name . ".jpg";
        file_put_contents($path,base64_decode($image));
        $ret = "SUCCESS";
        return $ret;
    }

    public function createLokasi($nama, $telp, $lat, $lang, $address, $statename, $countryname, $image, $video) {
        $ret = array();
        $image = "images/" . $image . ".jpg";
        $video = "images/" . $video;
        $query = "INSERT INTO lokasi (lokasi.nama, lokasi.telp, lokasi.lat, lokasi.lang, lokasi.address, lokasi.statename, lokasi.countryname, lokasi.images, lokasi.video) VALUES ('$nama', '$telp', '$lat', '$lang', '$address', '$statename', '$countryname', '$image', '$video')";
        if ($this->conn->query($query) === TRUE) {
            $result = "SUCCESS";
        } else {
            $result = "";
        }
        return $result;
    }

    public function getLokasi() {
        $ret = array();
        $index = 0;
        $query = "SELECT lokasi.id_lokasi, lokasi.nama, lokasi.telp, lokasi.lat, lokasi.lang, lokasi.address, lokasi.statename, lokasi.countryname, lokasi.images, lokasi.video FROM lokasi";
        $result = $this->conn->query($query);
        while ($task = $result->fetch_assoc()) {
            $tmp = array();
            $tmp["id_lokasi"] = $task["id_lokasi"];
            $tmp["nama"] = $task["nama"];
            $tmp["telp"] = $task["telp"];
            $tmp["lat"] = $task["lat"];
            $tmp["lang"] = $task["lang"];
            $tmp["address"] = $task["address"];
            $tmp["statename"] = $task["statename"];
            $tmp["countryname"] = $task["countryname"];
            $tmp["images"] = $task["images"];
            $tmp["video"] = $task["video"];
            array_push($ret, $tmp);
        }
        return $ret;
    }

    // public function getProperti() {
    //     $ret = array();
    //     $index = 0;
    //     $query = "SELECT * FROM properti";
    //     $result = $this->conn->query($query);
    //     while ($task = $result->fetch_assoc()) {
    //         $tmp = array();
    //         $tmp["id_properti"] = $task["id_properti"];
    //         $tmp["nama_properti"] = $task["nama_properti"];
    //         array_push($ret, $tmp);
    //     }
    //     return $ret;
    // }

	/**
	* Verifying required params posted or not
	*/
	function verifyRequiredParams($required_fields) {
		$error = false;
		$error_fields = "";
		$request_params = array();
		$request_params = $_REQUEST;
		
		// Handling PUT request params
		if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$app = \Slim\Slim::getInstance();
			parse_str($app->request()->getBody(), $request_params);
		}
		foreach ($required_fields as $field) {
			if  (!isset($request_params[$field])  || 
				strlen(trim($request_params[$field])) <= 0) {
				$error = true;
				$error_fields .= $field . ', ';
			}
		}
		if ($error) {
			// Required field(s) are missing or empty
			// echo error json and stop the app
			$response = array(); 
			$app = \Slim\Slim::getInstance();
			$response["error"] = true;
			$response["message"]  =  'Required  field(s)  '  . 
			substr($error_fields, 0, -2) . ' is missing or empty';
			echoRespnse(400, $response);
			$app->stop();
		}
	}
 
}
 
?>