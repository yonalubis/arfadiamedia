<?php
 
require_once 'include/DbHandler.php';
require_once 'include/PassHash.php';
require 'libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

/**
 * ----------- METHODS WITHOUT AUTHENTICATION ---------------------------------
 */
	$app->get('/log/:username/:password', function($username, $password) use ($app) {
        $db = new DbHandler();
        $result = $db->getUserByUsernameAndPassword($username, $password);
        if ($result != null) {
            $response = $result;
        } else {
            $response = "FALSE";
        }
        echoRespnse(200, $response);
    });
 
	$app->get('/login/:username/:password', function ($username, $password) use ($app) {
	    $response = "";
	    $db = new DbHandler();
	    $result = $db->Login($username, $password);
	    if ($result != null) {
	            $response = $result;
	        } else {
	            $response = "FALSE";
	        }
	    echoRespnse(200, $response);
	});

	$app->get('/register/:username/:password', function($username, $password) use ($app) {    
        $db = new DbHandler();
        $result = $db->addUser($username, $password);
        if ($result != null) {
            $response = $result;
        } else {
            $response = "FALSE";
        }
        echoRespnse(200, $response);
	});

	$app->get('/getproperti', function () use ($app) {
	    //$response = array();
	    $db = new DbHandler();
	    $result = $db->getProperti();
	    if ($result != null) {
	        $response = $result;
	    } else {
	        $response = "FALSE";
	    }
	    echoRespnse(200, $response);
	});

	$app->post('/uploadimage', function() use ($app) {
	    verifyRequiredParams(array('image', 'name'));
	    $response = array();
	    $name = $app->request->post('name');
	    $image = $app->request->post('image');
	    $db = new DbHandler();
	    $result = $db->SaveImage($image, $name);
	    if ($result != null) {
	        $response = $result;
	    } else {
	        $response = "FALSE";
	    }
	    echoRespnse(200, $response);
	});

	$app->get('/createlokasi/:nama/:telp/:lat/:lang/:address/:statename/:countryname/:image/:video', function($nama, $telp, $lat, $lang, $address, $statename, $countryname, $image, $video) use ($app) {
        
        $db = new DbHandler();
        $result = $db->createLokasi($nama, $telp, $lat, $lang, $address, $statename, $countryname, $image, $video);
        if ($result != null) {
            $response = $result;
        } else {
            $response = "FALSE";
        }
        echoRespnse(200, $response);
	});

	$app->get('/getlokasi', function () use ($app) {
	    $db = new DbHandler();
	    $response = array();
	    $response['error'] = false;
	    $response['lokasi'] = array();
	    $result = $db->getLokasi();
	    if ($result != null) {
	        $response['error'] = false;
	        $response['lokasi'] = $result;
	    } else {
	        $response['error'] = "true";
	    }
	    echoRespnse(200, $response);
	});


	// $app->get('/getproperti', function () use ($app) {
	//     $db = new DbHandler();
	//     $response = array();
	//     $response['error'] = false;
	//     $response['properti'] = array();
	//     $result = $db->getProperti();
	//     if ($result != null) {
	//         $response['error'] = false;
	//         $response['properti'] = $result;
	//     } else {
	//         $response['error'] = "true";
	//     }
	//     echoRespnse(200, $response);
	// });


	/**
	* Verifying required params posted or not
	*/
	function verifyRequiredParams($required_fields) {
		$error = false;
		$error_fields = "";
		$request_params = array();
		$request_params = $_REQUEST;
		
		// Handling PUT request params
		if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
			$app = \Slim\Slim::getInstance();
			parse_str($app->request()->getBody(), $request_params);
		}
		foreach ($required_fields as $field) {
			if  (!isset($request_params[$field])  || 
				strlen(trim($request_params[$field])) <= 0) {
				$error = true;
				$error_fields .= $field . ', ';
			}
		}
		if ($error) {
			// Required field(s) are missing or empty
			// echo error json and stop the app
			$response = array(); 
			$app = \Slim\Slim::getInstance();
			$response["error"] = true;
			$response["message"]  =  'Required  field(s)  '  . 
			substr($error_fields, 0, -2) . ' is missing or empty';
			echoRespnse(400, $response);
			$app->stop();
		}
	}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 * Daftar response
 * 200	OK
 * 201	Created
 * 304	Not Modified
 * 400	Bad Request
 * 401	Unauthorized
 * 403	Forbidden
 * 404	Not Found
 * 422	Unprocessable Entity
 * 500	Internal Server Error
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

	//print_r($response);
    echo json_encode($response);
}


$app->run();
?>